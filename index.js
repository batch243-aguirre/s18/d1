// console.log("Good afternoon!");

// Functions
	// Functions in javascript are lines blocks of codes that tell our device/application to perform a certain task when called/invoke.
	// Functions are mostly created to create complicated tasks to run several lines of codes in succession.
	// They are also used to prevent repeating lines/blocks of codes that contains the same task/function.

	// We also learned in the previous session that we can gather data from user using prompt window.

		/*function printInput(){
			let nickname = prompt("Enter your nickname:");
			console.log("Hi " + nickname);
		}

		printInput();*/

	// However, for some use cases, this may not be ideal.
	// For other cases, functions can also process data directly passed into it instead of relying on Global Variable ang prompt()
	
// Parameters and Arguments

		// consider this function
		function printName(name = "noName"){

			console.log("My name is " + name);
		}

		printName("Chris");

		printName();

		// You can directly pass data into the function. The function can then use that data which is referred as "name" within the function.

		// "name" is called parameter.
		// parameter acts as named variable/container that exist only inside of a function.

		// "Chris", the information/ data provided directly into the function called an argument.
		// Values passed when invoking a function are called arguments. These arguments are then stored as the parameters within the function.

	// variables can also be passed as an argument.
		let sampleVariable = "Edward";
		printName(sampleVariable);

	// function arguments cannot be used by a function if there are no parameters provided within the function.

		function noParams(){
			let params = "No parameter";

			console.log(params);
		}

		noParams("With parameter!");


		function checkDivisibilityBy8(num){
			let modulo = num%8;
			console.log("The remainder of " + num + " divided by 8 is: " + modulo);

			let isDivisibleBy8 = modulo === 0;
			console.log("Is " + num + " divisible by 8?");
			console.log(isDivisibleBy8);
		}

		checkDivisibilityBy8(8);

		checkDivisibilityBy8(17);

		// You can also do the same using the prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

	// Functions as Arguments
		// function parameters can also accept other functions as arguments.
		// Some complex functions use other functions as arguments to perform more complicated results.
		// This will be further seen when we discuss arrays methods.


		function argumentFunction(){
			console.log("This function was passed as an argument before the message was printed.");
		}

		function argumentFunctionTwo(){
			console.log("This function was passed as an argument from the second argument function.")
		}

		function invokeFunction(argFunction){
			console.log(argFunction);
		}

		invokeFunction(argumentFunction);

		invokeFunction(argumentFunctionTwo);

		// Adding and removing the parentheses "()" impacts the output of JavaScript heavily.
		// When function is used with parentheses "()", it denotes invoking a function.
		// A function used without parentheses "()" is normally associated with using the function as an argument to another function.

	// Using Multiple parameters
		// Multiple "arguments" will correspond to the number of "parameters" declared in a function in "succeeding order".

		function createFullName(firstName = "noFirstName", middleName = "noMiddleName", lastName = "noLastName"){
			console.log("This is firstName: " + firstName);
			console.log("This is middleName: " + middleName);
			console.log("This is lastName: " + lastName);
		}

		createFullName("Juan", "Dela", "Cruz");

		// "Juan" will be stored in the parameter "firstName"
		// "Dela" will be stored in the parameter "middleName"
		// "Cruz" will be stored in the parameter "lastName"

		// In JavaScript, providing more arguments than the expected parameters will not return an error.
		createFullName("Juan", "Dela", "Cruz", "Jr.");

		// Providing less arguments than the expected parameters will automatically assign an undefined value to the parameter.
		createFullName("Juan" ,"Dela Cruz");

		// using variables as arguments.

		let firstName = "John";
		let middleName = "Doe";
		let lastName = "Smith";

		createFullName(firstName, middleName, lastName);


	// return statement
		// The "return" statement allows us to ouput a value from a function to be passed to the line/block of code that invoked the function.
		
		function returnFullName(firstName, middleName, lastName){
			console.log(firstName + " " + middleName + " " + lastName);
		}

		returnFullName("Ada", "None", "Lovelace");

		function returnName(firstName, middleName, lastName){
			

			return firstName + " " + middleName + " " + lastName;

			console.log(firstName);
		}

		console.log(returnName("John", "Doe", "Smith"));

		let fullName = returnName("John", "Doe", "Smith");
		console.log("This is the console.log from fullName variable:")
		console.log(fullName);

		function printPlayerInfo(userName, level, job){
			console.log("UserName: " + userName);
			console.log("Level: " + level);
			console.log("Job: " + job);

			return userName + " " + level + " " + job;
		}

		printPlayerInfo("knight_white", 95, "Paladin");

		let user1 = printPlayerInfo("knight_white", 95, "Paladin");
		console.log(user1);
